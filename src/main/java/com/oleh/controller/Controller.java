package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.arrays.ArrayABC;
import com.oleh.model.arrays.MyDeque;
import com.oleh.model.arrays.TwoStrings;
import com.oleh.model.arrays.game.ArrayGame;
import com.oleh.model.generics.MyGenericSample;
import com.oleh.model.generics.PriorityQueue;

import java.util.List;

public class Controller implements ControllerInt{
    Model model;

    public Controller() {
        model = new Model();
    }

    @Override
    public MyGenericSample getMyGeneric() {
        return model.getMyGeneric();
    }

    @Override
    public void insertString(List list, String el) {
        list.add(el);
    }

    @Override
    public PriorityQueue getPriorityQueue() {
        return model.getMyPriorityQueue();
    }

    @Override
    public ArrayABC getArrayAbc() {
        return model.getArrayABC();
    }

    @Override
    public ArrayGame getArrayGame() {
        return model.getArrayGame();
    }

    @Override
    public long myListVsDefault() {
        return model.myListVsArrayListTime();
    }

    @Override
    public List<TwoStrings> getTwoStringsList() {
        return model.getTwoStringsList();
    }

    @Override
    public TwoStrings[] getTwoStringsArray() {
        return model.getTwoStringsArray();
    }

    @Override
    public MyDeque getDeque() {
        return model.getDeque();
    }
}
