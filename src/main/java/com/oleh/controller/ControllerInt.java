package com.oleh.controller;

import com.oleh.model.arrays.ArrayABC;
import com.oleh.model.arrays.MyDeque;
import com.oleh.model.arrays.TwoStrings;
import com.oleh.model.arrays.game.ArrayGame;
import com.oleh.model.generics.MyGenericSample;
import com.oleh.model.generics.PriorityQueue;

import java.util.List;

public interface ControllerInt {
    MyGenericSample getMyGeneric();
    void insertString(List list, String el);
    PriorityQueue getPriorityQueue();
    ArrayABC getArrayAbc();
    ArrayGame getArrayGame();
    long myListVsDefault();
    List<TwoStrings> getTwoStringsList();
    TwoStrings[] getTwoStringsArray();
    MyDeque getDeque();

}
