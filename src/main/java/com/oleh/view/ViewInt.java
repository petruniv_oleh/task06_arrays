package com.oleh.view;

public interface ViewInt {
    void menu();
    void subMenuArrays();
    void subMenuGenerics();

    void stringInsertingTest();
    void testGeneric();
    void testCustomPriorityQueue();
    void arrayABCTest();
    void arrayGameTest();
    void customListVsArrayList();
    void testClassWithComparator();
    void testDeque();
}
