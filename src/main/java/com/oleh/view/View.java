package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.arrays.ArrayABC;
import com.oleh.model.arrays.MyDeque;
import com.oleh.model.arrays.TwoStrings;
import com.oleh.model.arrays.game.ArrayGame;
import com.oleh.model.generics.MyGenericSample;
import com.oleh.model.generics.PriorityQueue;
import com.oleh.model.generics.objSample.Animal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View implements ViewInt {
    Controller controller;

    private final Logger logger = LogManager.getLogger(View.class.getName());

    public View() {
        logger.info("created view");
        controller = new Controller();
    }

    @Override
    public void menu() {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("\n Menu: ");
        System.out.println("==================");
        System.out.println("1. Array tasks");
        System.out.println("2. Generic tasks");
        System.out.println("3. Exit");
        System.out.println("==================");
        System.out.println("Enter a value: ");
        switch (sc.nextInt()) {
            case 1:
                logger.info("chose 1");
                subMenuArrays();
                break;
            case 2:
                logger.info("chose 2");
                subMenuGenerics();
                break;
            case 3:
                logger.info("chose 3");
                Runtime.getRuntime().exit(0);
                break;
            default:
                break;
        }
    }

    @Override
    public void subMenuArrays() {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("\n Menu: ");
        System.out.println("==================");
        System.out.println("1. Result for array logical tasks 1-3");
        System.out.println("2. Result for array game");
        System.out.println("3. Custom list vs Array list");
        System.out.println("4. Test class with comparator");
        System.out.println("5. Test custom Deque");
        System.out.println("6. Back");
        System.out.println("==================");
        System.out.println("Enter a value: ");
        switch (sc.nextInt()) {
            case 1:
                logger.info("chose 1");
                arrayABCTest();
                break;
            case 2:
                logger.info("chose 2");
                arrayGameTest();
                break;
            case 3:
                logger.info("chose 3");
                customListVsArrayList();
                break;
            case 4:
                logger.info("chose 4");
                testClassWithComparator();
                break;
            case 5:
                logger.info("chose 5");
                testDeque();
                break;
            case 6:
                logger.info("chose 6");
                menu();
                break;
            default:
                break;
        }
    }

    @Override
    public void subMenuGenerics() {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("\n Menu: ");
        System.out.println("==================");
        System.out.println("1. Test inserting String into List<Integer>");
        System.out.println("2. Test own generic class");
        System.out.println("3. Test custom priority queue");
        System.out.println("4. Back");
        System.out.println("==================");
        System.out.println("Enter a value: ");
        switch (sc.nextInt()) {
            case 1:
                logger.info("chose 1");
                stringInsertingTest();
                break;
            case 2:
                logger.info("chose 2");
                testGeneric();
                break;
            case 3:
                logger.info("chose 3");
                testCustomPriorityQueue();
                break;
            case 4:
                logger.info("chose 4");
                menu();
                break;
            default:
                break;
        }
    }

    @Override
    public void stringInsertingTest() {
        logger.info("inserting string into List<Integer>");
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(243);
        controller.insertString(integers, "This is how i insert string into List<Integers>");
        integers.add(211525);

        for (int i = 0; i < integers.size(); i++) {
            System.out.println(integers.get(i));
        }
        menu();
    }

    @Override
    public void testGeneric() {
        logger.info("Testing generic");
        MyGenericSample myGeneric = controller.getMyGeneric();
        for (int i = 0; i < myGeneric.size(); i++) {
            System.out.println(myGeneric.getUnit(i));
        }

        myGeneric.findAnimal(new Animal("elephant"));

        menu();

    }

    @Override
    public void testCustomPriorityQueue() {
        logger.info("testing custom prior queue");
        PriorityQueue priorityQueue = controller.getPriorityQueue();
        System.out.println("size: " + priorityQueue.getSize());
        System.out.println("\nMax prior: " + priorityQueue.getMaxPrior());
        System.out.println("size: " + priorityQueue.getSize());
        System.out.println("\nnext: " + priorityQueue.getMaxPrior());
        System.out.println("size: " + priorityQueue.getSize());
        System.out.println("\nnext: " + priorityQueue.getMaxPrior());
        System.out.println("size: " + priorityQueue.getSize());
        System.out.println("\nnext: " + priorityQueue.getMaxPrior());
        System.out.println("size: " + priorityQueue.getSize());
        menu();
    }

    @Override
    public void arrayABCTest() {
        logger.info("testing array A, B and C tasks");
        ArrayABC arrayAbc = controller.getArrayAbc();
        System.out.println("task A: ");
        System.out.print("\nFirst arr: ");
        printArr(arrayAbc.getFirsArray());
        System.out.print("\nSecond arr: ");
        printArr(arrayAbc.getSecondArray());
        System.out.println("\nElements in both arrays");
        printArr(arrayAbc.getIntersectedArray());
        System.out.println("\nElements unique for first array");
        printArr(arrayAbc.getUniqueArray());
        System.out.println("\ntask B");
        System.out.println("\narray for B");
        printArr(arrayAbc.getArrForBTask());
        System.out.println("\nArray without 3 or more repeats one");
        printArr(arrayAbc.getResultArrForBTask());
        System.out.println("\ntask C");
        System.out.println("\narray for C");
        printArr(arrayAbc.getArrForCTask());
        System.out.println("\nArray without repeats one by another");
        printArr(arrayAbc.getResForCTask());
        menu();
    }

    @Override
    public void arrayGameTest() {
        logger.info("testing game test");
        ArrayGame arrayGame = controller.getArrayGame();
        arrayGame.printDoors();
        System.out.println("Death doors " + arrayGame.countDeadEnds(0, 0));
        arrayGame.printWinStrategy();
        menu();
    }

    @Override
    public void customListVsArrayList() {
        logger.info("compare custom list vs default ArrayList");
        long l = controller.myListVsDefault();
        if (l > 0) {
            System.out.println("My custom string list is " + l + " nano sec faster ");
        }
        if (l == 0) {
            System.out.println("They have same speed of work");
        }
        if (l < 0) {
            System.out.println("ArrayList is " + Math.abs(l) + " faster");
        }
        menu();
    }

    @Override
    public void testClassWithComparator() {
        logger.info("test custom comparator");
        TwoStrings[] twoStringsArray = controller.getTwoStringsArray();
        List<TwoStrings> twoStringsList = controller.getTwoStringsList();
        System.out.println("List before sorting");
        printList(twoStringsList);
        System.out.println("Array before sorting");
        printCustomArr(twoStringsArray);
        Arrays.sort(twoStringsArray);
        Collections.sort(twoStringsList);

        System.out.println("List after sorting");
        printList(twoStringsList);
        System.out.println("Array after sorting");
        printCustomArr(twoStringsArray);

        TwoStrings twoStrings = new TwoStrings("Japan", "Tokio");

        for (int i = 0; i < twoStringsArray.length; i++) {
            if (twoStringsArray[i].compareTo(twoStrings) == 0) {
                System.out.println("FoundInArr");
            }
            if (twoStringsList.get(i).compareTo(twoStrings) == 0) {
                System.out.println("FoundInList");
            }
        }

        menu();

    }


    private void printList(List list) {
        logger.info("printing list");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

    }

    private void printCustomArr(TwoStrings[] arr) {
        logger.info("printing custom array");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

    }

    @Override
    public void testDeque() {
        logger.info("test my deque");
        MyDeque deque = controller.getDeque();
        System.out.println("First " + deque.getFirs());
        System.out.println("Last " + deque.getLast());

        System.out.println("Remove first" + deque.removeFirst());
        System.out.println("First " + deque.getFirs());
        System.out.println("Remove last " + deque.removeLast());
        System.out.println("Last " + deque.getLast());

        menu();
    }


    private void printArr(int[] arr) {
        logger.info("print int array");
        for (int i = 0; i < arr.length; i++) {
            System.out.print("  " + arr[i]);
        }

    }
}
