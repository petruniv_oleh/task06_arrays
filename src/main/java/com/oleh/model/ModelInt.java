package com.oleh.model;

import com.oleh.model.arrays.ArrayABC;
import com.oleh.model.arrays.MyDeque;
import com.oleh.model.arrays.TwoStrings;
import com.oleh.model.arrays.game.ArrayGame;
import com.oleh.model.generics.MyGenericSample;
import com.oleh.model.generics.PriorityQueue;

import java.util.List;

public interface ModelInt {
    MyGenericSample getMyGeneric();
    PriorityQueue getMyPriorityQueue();
    ArrayABC getArrayABC();
    ArrayGame getArrayGame();
    long myListVsArrayListTime();
    List<TwoStrings> getTwoStringsList();
    TwoStrings[] getTwoStringsArray();
    MyDeque getDeque();

}
