package com.oleh.model.arrays;

import java.util.Random;

public class ArrayABC {
    private int[] firsArray;
    private int[] secondArray;
    private int[] arrForBTask;
    private int[] arrForCTask;

    public ArrayABC() {
        firsArray = arrayInit(true, 10);
        secondArray = arrayInit(true, 10);
        arrForBTask = arrayInit(false, 10);
        arrForCTask = arrayInit(false, 15);
    }

    private int[] arrayInit(boolean unique, int maxLenth) {
        Random random = new Random();
        int[] arr = new int[random.nextInt(maxLenth) + 2];

        for (int i = 0; i < arr.length; i++) {
            int generatedNumber;
            if (unique) {
                do {
                    generatedNumber = random.nextInt(20);
                } while (isInArray(arr, generatedNumber));
            } else {
                generatedNumber = random.nextInt(8);
            }
            arr[i] = generatedNumber;
        }

        return arr;
    }

    private boolean isInArray(int[] arr, int el) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == el) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return array which contains elements that are common for both arrays
     */
    public int[] getIntersectedArray() {
        int[] thirdArray = new int[countSimilar(firsArray, secondArray)];
        int k = 0;
        for (int i = 0; i < firsArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {
                if (firsArray[i] == secondArray[j]) {
                    thirdArray[k] = firsArray[i];
                    k++;
                }
            }
        }
        return thirdArray;
    }

    /**
     * @return array which contains values from firstArray and are not in secondArray
     */
    public int[] getUniqueArray() {
        int[] thirdArray = new int[countDifferent(firsArray, secondArray)];
        boolean isUnique;
        int k = 0;
        for (int i = 0; i < firsArray.length; i++) {
            isUnique = true;
            for (int j = 0; j < secondArray.length; j++) {
                isUnique &= (firsArray[i] != secondArray[j]);
            }
            if (isUnique) {
                thirdArray[k] = firsArray[i];
                k++;
            }
        }

        return thirdArray;
    }

    /**
     * Without numb that repeat more than 2 times
     *
     * @return return array without values thar are repeated more than 2 times
     */
    public int[] getResultArrForBTask() {
        int[] resultArr;
        int[] tempArr = new int[arrForBTask.length];
        int k = 0;
        for (int i = 0; i < arrForBTask.length; i++) {
            if (countRepeats(arrForBTask, arrForBTask[i]) <= 2) {
                tempArr[k] = arrForBTask[i];
                k++;
            }
        }

        resultArr = copyArr(k, tempArr);
        return resultArr;

    }

    private int countRepeats(int[] arr, int val) {
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                counter++;
            }
        }
        return counter;
    }

    public int[] getArrForBTask() {
        return arrForBTask;
    }

    private int countSimilar(int[] arr1, int[] arr2) {
        int counter = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    counter++;
                }
            }
        }
        return counter;
    }

    private int countDifferent(int[] arr1, int[] arr2) {
        int counter = 0;
        boolean checker;
        for (int i = 0; i < arr1.length; i++) {
            checker = true;
            for (int j = 0; j < arr2.length; j++) {
                checker &= (arr1[i] != arr2[j]);
            }
            if (checker) {
                counter++;
            }
        }
        return counter;
    }

    /**
     *
     * @return array without repeating values that following each other
     */
    public int[] getResForCTask() {
        int[] resultArr;
        int[] tempArr = new int[arrForCTask.length];

        int currVal = arrForCTask[0];
        tempArr[0] = currVal;
        int k = 1;
        for (int i = 1; i < arrForCTask.length; i++) {
            if (currVal == arrForCTask[i]) {
                continue;
            } else {
                currVal = arrForCTask[i];
                tempArr[k] = currVal;
                k++;

            }
        }

        resultArr = copyArr(k, tempArr);

        return resultArr;
    }

    private int[] copyArr(int k, int[] arr) {
        int[] resArr = new int[k];
        for (int i = 0; i < resArr.length; i++) {
            resArr[i] = arr[i];
        }
        return resArr;
    }

    public int[] getFirsArray() {
        return firsArray;
    }

    public int[] getSecondArray() {
        return secondArray;
    }

    public int[] getArrForCTask() {
        return arrForCTask;
    }
}
