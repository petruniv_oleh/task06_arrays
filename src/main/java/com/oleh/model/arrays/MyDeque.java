package com.oleh.model.arrays;

import java.util.ArrayList;
import java.util.List;

public class MyDeque<T> {
    private List<T> dequeList;

    public MyDeque() {
        dequeList = new ArrayList<>();
    }

    public void pushBack(T el) {
        dequeList.add(el);
    }

    public void pushFront(T el) {
        dequeList.add(0, el);
    }

    public T getFirs(){
        return dequeList.get(0);
    }

    public T getLast(){
        return dequeList.get(dequeList.size()-1);
    }

    public boolean removeFirst(){
        if (dequeList.remove(0)!=null) {
            return true;
        }
        return false;
    }

    public boolean removeLast(){
        if (dequeList.remove(dequeList.size()-1)!=null) {
            return true;
        }
        return false;
    }

    public int size(){
        return dequeList.size();
    }
}
