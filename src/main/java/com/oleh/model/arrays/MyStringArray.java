package com.oleh.model.arrays;

public class MyStringArray  extends ArrayABC{
    private String[] arr;

    public MyStringArray() {
        arr = new String[10];
    }



    public void add(String s) {
        if (size() == arr.length) {
            enlargeArray();
        }

        arr[size()] = s;

    }

    public String get(int i){
        return arr[i];
    }
    private void enlargeArray() {
        String[] tempArr = new String[arr.length * 2];

        for (int i = 0; i < arr.length; i++) {
            tempArr[i] = arr[i];
        }

        arr = tempArr;
    }

    public int size() {
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                counter++;
            }
        }
        return counter;
    }

}
