package com.oleh.model.arrays;

import org.jetbrains.annotations.NotNull;

public class TwoStrings implements Comparable<TwoStrings > {
    private String s1;
    private String s2;

    public TwoStrings(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public int compareTo(@NotNull TwoStrings o) {
        return s1.compareTo(o.s1);
    }

    @Override
    public String toString() {
        return "TwoStrings{" +
                "s1='" + s1 + '\'' +
                ", s2='" + s2 + '\'' +
                '}';
    }
}
