package com.oleh.model.arrays.game;

public class Door {
    private int doorVal;
    private DoorType doorType;
    private boolean haveBeenOpened;

    public Door(int doorVal, DoorType doorType) {
        this.doorVal = doorVal;
        this.doorType = doorType;
        haveBeenOpened = false;
    }

    public int getDoorVal() {
        return doorVal;
    }

    public DoorType getDoorType() {
        return doorType;
    }

    public boolean isHaveBeenOpened() {
        return haveBeenOpened;
    }

    public void setHaveBeenOpened(boolean haveBeenOpened) {
        this.haveBeenOpened = haveBeenOpened;
    }
}
