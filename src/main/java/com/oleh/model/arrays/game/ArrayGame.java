package com.oleh.model.arrays.game;

import java.util.Random;

public class ArrayGame {
    private final int AMOUNT_OF_DOORS = 10;
    private int heroForce;
    private Door[] doors;


    public ArrayGame() {
        this.heroForce = 25;
        this.doors = new Door[AMOUNT_OF_DOORS];
        doorsInit();
    }

    /**
     * This method creates random doors and putting it into door array
     */
    private void doorsInit() {
        Random random = new Random();
        for (int i = 0; i < doors.length; i++) {
            DoorType doorType;
            int doorVal;
            if (random.nextInt(2) == 1) {
                doorType = DoorType.ARTEFACT;
                doorVal = random.nextInt(70) + 10;
            } else {
                doorType = DoorType.MONSTER;
                doorVal = random.nextInt(95) + 5;
            }

            doors[i] = new Door(doorVal, doorType);


        }

    }

    /**
     * Recursive counter of doors where hero will fall
     * @param i - array pointer
     * @param deadEnds - bad doors counter
     * @return amount of dead doors
     */
    public int countDeadEnds(int i, int deadEnds) {
        if (doors[i].getDoorType().equals(DoorType.MONSTER)
                && doors[i].getDoorVal() > heroForce) {
            deadEnds++;
        }
        i++;
        if (i < AMOUNT_OF_DOORS) {
            return countDeadEnds(i, deadEnds);
        }
        return deadEnds;
    }

    /**
     * Simple algorithm to get win strategy
     */
    public void printWinStrategy() {
        System.out.print("To can open the doors like: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].isHaveBeenOpened()) {
                continue;
            }
            if (doors[i].getDoorType().equals(DoorType.MONSTER)) {
                if (doors[i].getDoorVal() > heroForce) {
                    continue;
                } else {
                    doors[i].setHaveBeenOpened(true);
                    System.out.print("  №" + (i + 1));
                }
            }
            if (doors[i].getDoorType().equals(DoorType.ARTEFACT)) {
                doors[i].setHaveBeenOpened(true);
                System.out.print("  №" + (i + 1));
                heroForce += doors[i].getDoorVal();
                i = 0;
            }
        }
        if (openedDoorsCounter() != 10) {
            System.out.println("\nbut then you will die");
        }

    }

    private int openedDoorsCounter() {
        int counter = 0;
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].isHaveBeenOpened()) {
                counter++;
            }
        }
        return counter;
    }

    public void printDoors() {
        System.out.println("Doors: ");
        for (int i = 0; i < doors.length; i++) {
            System.out.print("   " + (i + 1) + ". " + doors[i].getDoorType() + "(" + doors[i].getDoorVal() + ")");
        }
    }
}
