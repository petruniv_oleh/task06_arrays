package com.oleh.model.generics.objSample;

import java.util.Objects;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                '}';
    }
}
