package com.oleh.model.generics.objSample;

public class Fish extends Animal{


    public Fish(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Fish{"+super.toString()+"}";
    }
}
