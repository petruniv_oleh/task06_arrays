package com.oleh.model.generics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PriorityQueue<T> {

    class Node {
        T el;
        int priority;
        public Node(T el, int priority) {
            this.el = el;
            this.priority = priority;
        }

    }

    private List<Node> queue;

    public PriorityQueue() {
        queue = new ArrayList<Node>();
    }

    public boolean isEmpty() {
        if (queue.size() == 0) {
            return true;
        } else {
            return false;
        }

    }

    public void insert(T el, int priority) {
        queue.add(new Node(el, priority));
        queue.sort(new Comparator<Node>() {
            public int compare(Node o1, Node o2) {
                return o2.priority-o1.priority;
            }
        });
    }

    public T getMaxPrior(){
        Node node = queue.get(0);
        queue.remove(0);
        return node.el;
    }

    public int getSize(){
        return queue.size();
    }

}
