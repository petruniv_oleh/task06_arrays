package com.oleh.model.generics;



import com.oleh.model.generics.objSample.Animal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyGenericSample<T> {
    private List<T> units;

    public MyGenericSample() {
        units = new ArrayList<T>();
    }

    public T getUnit(int i) {
        return units.get(i);
    }

    public void putUnit(T unit) {
        units.add(unit);
    }

    public <U extends Animal> void findAnimal(U animal){
        Iterator<T> iterator = units.iterator();
        while (iterator.hasNext()){
            if (iterator.next().equals(animal)){
                System.out.println(animal.toString()+" is in list");
            }
        }
    }

    public int size(){
        return units.size();
    }

}
