package com.oleh.model;

import com.oleh.model.arrays.ArrayABC;
import com.oleh.model.arrays.MyDeque;
import com.oleh.model.arrays.MyStringArray;
import com.oleh.model.arrays.TwoStrings;
import com.oleh.model.arrays.game.ArrayGame;
import com.oleh.model.generics.MyGenericSample;
import com.oleh.model.generics.PriorityQueue;
import com.oleh.model.generics.objSample.Animal;
import com.oleh.model.generics.objSample.Fish;

import java.util.ArrayList;
import java.util.List;


public class Model implements ModelInt {


    @Override
    public MyGenericSample getMyGeneric() {
        MyGenericSample<Animal> myGenericSample = new MyGenericSample<>();
        myGenericSample.putUnit(new Fish("shark"));
        myGenericSample.putUnit(new Fish("okun"));
        myGenericSample.putUnit(new Fish("pirania"));
        myGenericSample.putUnit(new Animal("elephant"));

        return myGenericSample;
    }

    @Override
    public PriorityQueue getMyPriorityQueue() {
        PriorityQueue<String> queue = new PriorityQueue<>();
        queue.insert("king", 5);
        queue.insert("citizen", 2);
        queue.insert("villager", 1);
        queue.insert("lord", 4);

        return queue;
    }

    @Override
    public ArrayABC getArrayABC() {
        return new ArrayABC();
    }

    @Override
    public ArrayGame getArrayGame() {
        return new ArrayGame();
    }

    @Override
    public long myListVsArrayListTime() {
        MyStringArray stringArray = new MyStringArray();
        List<String> stringList = new ArrayList<>();
        long myTimeStart;
        long myTimeEnd;
        long myTime;
        long defaultStart;
        long defaultEnd;
        long defaultTime;

        myTimeStart = System.nanoTime();
        for (int i = 0; i < 50; i++) {
            stringArray.add("string" + i);
        }
        myTimeEnd = System.nanoTime();
        myTime = myTimeEnd - myTimeStart;

        defaultStart = System.nanoTime();
        for (int i = 0; i < 50; i++) {
            stringList.add("string" + i);
        }
        defaultEnd = System.nanoTime();
        defaultTime = defaultEnd - defaultStart;

        return myTime - defaultTime;
    }

    @Override
    public List<TwoStrings> getTwoStringsList() {
        List<TwoStrings> twoStrings = new ArrayList<>();
        twoStrings.add(new TwoStrings("Ukraine", "Kiyv"));
        twoStrings.add(new TwoStrings("Japan", "Tokio"));
        twoStrings.add(new TwoStrings("Germany", "Berlin"));

        return twoStrings;
    }

    @Override
    public TwoStrings[] getTwoStringsArray() {
        TwoStrings[] twoStrings = new TwoStrings[3];
        twoStrings[0] = new TwoStrings("Ukraine", "Kiyv");
        twoStrings[1] = new TwoStrings("Japan", "Tokio");
        twoStrings[2] = new TwoStrings("Germany", "Berlin");

        return twoStrings;
    }

    @Override
    public MyDeque getDeque() {
        MyDeque<String> stringMyDeque = new MyDeque<>();
        stringMyDeque.pushFront("1FrontPush");
        stringMyDeque.pushFront("2FrontPush");
        stringMyDeque.pushBack("1BackPush");
        stringMyDeque.pushFront("3FrontPush");
        stringMyDeque.pushBack("2BackPush");

        return stringMyDeque;
    }
}
